terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.67.0"
    }
  }
  backend "s3" {
    # bucket and key must match those used in the global and version settings modules
    bucket = "summit-terraform-state"
    key = "aws-gitlab-runner/base.json"
  }
}

provider "aws" {
  default_tags {
    tags = {
      Owner = "aws-gitlab-runner"
      Environment = "qa"
    }
  }
}

data aws_caller_identity current {}
data aws_region current {}
