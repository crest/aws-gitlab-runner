resource aws_iam_role worker {
  name = "gitlab-runner-worker-role"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
}

resource aws_iam_role_policy_attachment ecr_read_only_policy_attach {
  role = aws_iam_role.worker.name
  policy_arn = data.aws_iam_policy.ecr_read_only_access.arn
}

resource aws_iam_instance_profile worker {
  name = "gitlab-runner-worker-profile"
  role = aws_iam_role.worker.name
}


