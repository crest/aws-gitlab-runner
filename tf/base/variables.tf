variable runner_instance_type {
  type = string
  description = "AWS EC2 instance class name for Gitlab runner"
  default = "r5.large"
}

variable runner_instance_type_ui_ft {
  type = string
  description = "AWS EC2 instance class name for UI FT Gitlab runner"
  default = "m7a.xlarge"
}

locals {
  runner_ecr_url                = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${data.aws_region.current.id}.amazonaws.com"
  runner_environment            = "qa"
  runner_gitlab_runner_version  = "v15.8.3"
  runner_idle_time              = 3600
  runner_max_builds             = 30
  runner_responsible_party      = "CREST"
  runner_ssh_key_name           = "crest-key-1"
  runner_vcs                    = "git@code.vt.edu:crest/aws-gitlab-runner"
}
