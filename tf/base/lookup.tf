data aws_iam_policy ecr_read_only_access {
  name = "AmazonEC2ContainerRegistryReadOnly"
}


data aws_vpcs qa_infra {
  tags = {
    Name = "summit-qa-infra"
  }
}

locals {
  vpc_id = data.aws_vpcs.qa_infra.ids[0]
}

data aws_subnets qa_infra {
  filter {
    name   = "vpc-id"
    values = [local.vpc_id]
  }
  tags = {
    Type = "public"
  }
}
