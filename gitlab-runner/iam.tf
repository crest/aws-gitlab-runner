resource "aws_iam_role" "role" {
  name               = "${var.runner_name}-gitlab-runner"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = local.service_tags
}

# This policy gives our bastion access to resources
resource "aws_iam_policy" "policy" {
  name = "${var.runner_name}-gitlab-runner-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "arn:aws:s3:::${var.gitlab_runner_cache_name}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:CreateTags",
        "ec2:DescribeInstances",
        "ec2:DescribeKeyPairs",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:ImportKeyPair",
        "ec2:DeleteKeyPair",
        "ec2:RunInstances",
        "ec2:StopInstances",
        "ec2:TerminateInstances",
        "ec2messages:GetMessages",
        "ssm:GetDocument",
        "ssm:ListInstanceAssociations",
        "ssm:PutComplianceItems",
        "ssm:PutInventory",
        "ssm:UpdateInstanceAssociationStatus",
        "ssm:UpdateInstanceInformation"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "attach" {
  name       = "attachment"
  roles      = [aws_iam_role.role.name]
  policy_arn = aws_iam_policy.policy.arn
}

data "aws_iam_policy" "ecr_read_only_access" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "ecr_read_only_policy_attach" {
  role       = aws_iam_role.role.name
  policy_arn = data.aws_iam_policy.ecr_read_only_access.arn
}

resource "aws_iam_instance_profile" "profile" {
  name = "${var.runner_name}-gitlab-runner-profile"
  role = aws_iam_role.role.name
}

data "aws_iam_instance_profile" "worker_machine" {
  name  = var.machine_iam_instance_profile
}

resource "aws_iam_policy" "runner_pass_role_to_worker" {
  name   = "${var.runner_name}-gitlab-runner-policy-pass-role-to-worker"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "iam:PassRole"
      ],
      "Resource": [
        "${data.aws_iam_instance_profile.worker_machine.role_arn}"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "runner_pass_role_to_worker" {
  name       = "${var.runner_name}-runner-pass-role-to-worker"
  roles      = [aws_iam_role.role.name]
  policy_arn = aws_iam_policy.runner_pass_role_to_worker.arn
}
